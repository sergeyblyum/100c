//
//  BaseTableViewCell.swift
//  100С
//
//  Created by user on 21/03/2020.
//  Copyright © 2020 baccasoft. All rights reserved.
//

import Foundation

protocol BaseTableViewCell {
    func configureWithCellObject(cellObject: BaseTableViewCellObject)
}
