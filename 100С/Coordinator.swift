//
//  Coordinator.swift
//  100C
//
//  Created by Алихан Пешхоев on 09/07/2019.
//  Copyright © 2019 Алихан Пешхоев. All rights reserved.
//

import Foundation
import UIKit

class Coordinator {
    
    //MARK: Properties
    private var navController = UINavigationController()
    
    
    //MARK: UI
    
    func configureNavigationController() {

        let navigationBarAppearance = UINavigationBar.appearance()

        // Configure NavigationBar
        navigationBarAppearance.barTintColor = .white
        navigationBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.black]
        navigationBarAppearance.tintColor = UIColor.black
        navigationBarAppearance.isTranslucent = false

        //убрали сепаратор
        navigationBarAppearance.shadowImage = UIImage()
        navigationBarAppearance.setBackgroundImage(UIImage(), for: .default)

        navigationBarAppearance.topItem?.hidesBackButton = false

    }
    
}


//MARK: CoordinatorType

extension Coordinator: CooordinatorType {
    
    func start() {
        self.navController = UINavigationController()
        
        UIApplication.shared.delegate?.window??.rootViewController = self.navController;
        UIApplication.shared.delegate?.window??.rootViewController?.view.layoutSubviews()
        self.configureNavigationController()
        
        
        
    }
    
    func navigationController() -> UINavigationController? {
        return self.navController
    }
    
}

//MARK: MainScreenCoordinatorOutput

extension Coordinator: MainScreenCoordinatorOutput{
    func didFinishMainScreenCoordinator() {
        print("mainScreenCoordinator finished")
    }
}


//MARK: Routes

extension Coordinator {
    
    private func startMainCoordinator() {
        let mainScreenCoordinator = MainScreenCoordinatorAssembly.build(output: self)
        mainScreenCoordinator.start()
    }
}
