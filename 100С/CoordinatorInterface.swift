


import Foundation
import UIKit

@objc protocol CooordinatorType {
    func start()
    func navigationController() -> UINavigationController?
}
