//
//  MainMenuAssembler.swift
//  100С
//
//  Created by user on 10/01/2020.
//  Copyright © 2020 baccasoft. All rights reserved.
//

import Foundation
import UIKit

class MainMenuAssembler {
    static func assembly() -> UIViewController {
        let presenter = MainMenuPresenter()
        let vc = MainMenuVC()
        presenter.view = vc
        presenter.factory = MenuItemsFactory()
        vc.output = presenter
        return vc
    }
}
