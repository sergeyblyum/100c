//
//  MainMenuPresenter.swift
//  100С
//
//  Created by user on 10/01/2020.
//  Copyright © 2020 baccasoft. All rights reserved.
//

import Foundation

class MainMenuPresenter: MainMenuOutput{
    
    var factory: MenuItemsFactory?
    weak var view: MainMenuView?
    
    func didTriggerViewReadyEvent() {
        
        if let cellObjects = self.factory?.getCellObjects() {
            self.view?.configureModule(cellObjects: cellObjects)
        }
    }
    
   
    
}
