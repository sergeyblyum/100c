//
//  MainMenuProtocols.swift
//  100С
//
//  Created by user on 10/01/2020.
//  Copyright © 2020 baccasoft. All rights reserved.
//

import Foundation

protocol MainMenuOutput: class {
    func didTriggerViewReadyEvent()
}

protocol MainMenuView: class {
    func configureModule(cellObjects: [BaseTableViewCellObject])
}
