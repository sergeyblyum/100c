//
//  MainMenuVC.swift
//  100С
//
//  Created by user on 10/01/2020.
//  Copyright © 2020 baccasoft. All rights reserved.
//

import Foundation
import UIKit

class MainMenuVC: UIViewController, MainMenuView {
   
    @IBOutlet weak var menuTableView: UITableView!
    var output: MainMenuOutput?
    
    var cellObjects: [BaseTableViewCellObject] = [BaseTableViewCellObject]()
    
    override func viewDidLoad() {
        self.menuTableView.delegate = self
        self.menuTableView.dataSource = self
        self.setupView()
        self.output?.didTriggerViewReadyEvent()
    }
    
    func setupView() {
        
    }
    
    func configureModule(cellObjects: [BaseTableViewCellObject]) {
        self.cellObjects = cellObjects
        self.menuTableView.reloadData()
    }
       
       
}

extension MainMenuVC: UITableViewDelegate {
    
}

extension MainMenuVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellObjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellObject = self.cellObjects[indexPath.row]
         var cell = self.menuTableView.dequeueReusableCell(withIdentifier: cellObject.cellReuseIdentifier()) as? BaseTableViewCell
            
         if cell == nil {
            self.menuTableView.register(cellObject.cellNib(), forCellReuseIdentifier:
             cellObject.cellReuseIdentifier())
             cell = self.menuTableView.dequeueReusableCell(withIdentifier:
                 cellObject.cellReuseIdentifier()) as? BaseTableViewCell
         }

         if let menuCell = cell as? MainMenuCell {
             menuCell.configureWithCellObject(cellObject: cellObject)
             return menuCell
         }
        
        return UITableViewCell()
    }
}
