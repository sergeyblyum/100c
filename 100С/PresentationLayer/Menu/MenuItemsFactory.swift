//
//  MenuItemsFactory.swift
//  100С
//
//  Created by user on 21/03/2020.
//  Copyright © 2020 baccasoft. All rights reserved.
//

import Foundation
class MenuItemsFactory {
    func getCellObjects() -> [BaseTableViewCellObject] {
        var cellObjects = [BaseTableViewCellObject]()
        
        let rating = MainMenuCellObject.init(title: "Рейтинг", iconName: "icon_rating")
        cellObjects.append(rating)
        
        return cellObjects
    }
}
