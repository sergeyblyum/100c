//
//  MainMenuCell.swift
//  100С
//
//  Created by user on 10/01/2020.
//  Copyright © 2020 baccasoft. All rights reserved.
//

import Foundation
import UIKit

class MainMenuCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    
}

extension MainMenuCell: BaseTableViewCell {
    func configureWithCellObject(cellObject: BaseTableViewCellObject) {
        if let object = cellObject as? MainMenuCellObject {
            self.titleLabel.text = object.title
            self.iconImageView.image = UIImage(named: object.iconName)
        }
    }
}
