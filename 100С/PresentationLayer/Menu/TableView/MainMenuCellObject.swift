//
//  MainMenuCellObject.swift
//  100С
//
//  Created by user on 21/03/2020.
//  Copyright © 2020 baccasoft. All rights reserved.
//

import Foundation
import UIKit

class MainMenuCellObject {
    //MARK: Constants
    private final let kCellReuseIdentifier = "MainMenuCell"
    
    let title: String
    let iconName: String
    
    init(title: String, iconName: String) {
        self.title = title
        self.iconName = iconName
    }

}

extension MainMenuCellObject: BaseTableViewCellObject {
    
    func cellReuseIdentifier() -> String {
        return kCellReuseIdentifier
    }
    
    func height() -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func cellNib() -> UINib {
        return UINib(nibName: kCellReuseIdentifier, bundle: nil)
    }
    
}
